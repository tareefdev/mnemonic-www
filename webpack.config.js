const path = require("path");

module.exports = [
  {
    module: {
      rules: [
        {
          test: /gatsby\/cache-dir.*\.js$/,
          use: {
            loader: "babel-loader",
            options: {
              presets: ["babel-preset-gatsby"]
            }
          }
        },
        {
          test: /\.js$/,
          exclude: /(node_modules|bower_components)/,
          use: {
            loader: "babel-loader",
            options: {
              presets: ["@babel/preset-react", "@babel/preset-env"]
            }
          }
        },
        {
          test: /\.css$/,
          use: ["style-loader", "css-loader"]
        },
        {
          test: /\.scss$/,
          use: ["style-loader", "css-loader", "sass-loader"]
        }
      ]
    },
    entry: "./src/index.js",
    output: {
      filename: "index.js",
      path: path.resolve(__dirname, "lib"),
      library: "mnemonic-www",
      libraryTarget: "umd"
    },
    externals: {
      react: "React",
      "@emotion/styled": "styled",
      "@emotion/core": "core",
      "prop-types": "PropTypes",
      gatsby: "gatsby",
      typography: "Typography",
      "gatsby-image": "Img",
      "react-dom": "ReactDom",
      lodash: "lodash"
    }
  }
];
