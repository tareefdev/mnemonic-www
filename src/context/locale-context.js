import React from "react";
import { locales } from "../config/i18n";
const languages = Object.keys(locales);

const defaultLocale = "en";
const isBuilding = typeof window === `undefined`;
const locale =
  isBuilding || window.location.pathname.startsWith("/ar") ? "ar" : "en";

const LocaleContext = React.createContext(locale);
const LocaleConsumer = LocaleContext.Consumer;

const defaultLocation = {};
const LocationContext = React.createContext(locale);
const LocationConsumer = LocationContext.Consumer;

// const wrapPageElement = ({ element, props }) => {
//   const { pathname } = props.location;
//   const locale = pathname.startsWith("/ar") ? "ar" : "en";
//   const contextValue = locale;

//   return (
//     <LocationContext.Provider value={props.location}>
//       <LocaleContext.Provider value={contextValue}>
//         {element}
//       </LocaleContext.Provider>
//     </LocationContext.Provider>
//   );
// };

export {
  //  wrapPageElement,
  LocaleConsumer,
  LocaleContext,
  languages,
  LocationContext,
  LocationConsumer
};
