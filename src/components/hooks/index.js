import useDataApi from "./useDataApi";
//import useWindowSize from "./useWindowSize";

const hooks = { useDataApi };

export default hooks;
