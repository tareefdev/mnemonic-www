import React from "react";
import translations from "../../config/translations/translations.json";

export default function useTranslations(locale) {
  return function tr(s) {
    try {
      return translations[locale][s.toLowerCase()]
        ? translations[locale][s.toLowerCase()]
        : s;
    } catch (e) {
      return s;
    }
  };
}
