import React from "react";
import PropTypes from "prop-types";
import Img from "gatsby-image";

const PreviewCompatibleImage = ({ imageInfo, imageStyle }) => {
  const { alt = "", childImageSharp, image } = imageInfo;
  const imgType = image.childImageSharp.fluid ? "fluid" : "fixed";
  const imgValue = image.childImageSharp.fluid
    ? image.childImageSharp.fluid
    : image.childImageSharp.fixed;
  const dynamicImgProp = { [imgType]: imgValue };

  if (!!image && !!image.childImageSharp) {
    return <Img css={imageStyle} alt={alt} {...dynamicImgProp} />;
  }

  if (!!childImageSharp) {
    return <Img css={imageStyle} alt={alt} {...dynamicImgProp} />;
  }

  if (!!image && typeof image === "string")
    return <img css={imageStyle} src={image} alt={alt} />;

  return null;
};

PreviewCompatibleImage.propTypes = {
  imageInfo: PropTypes.shape({
    alt: PropTypes.string,
    childImageSharp: PropTypes.object,
    image: PropTypes.oneOfType([PropTypes.object, PropTypes.string]).isRequired,
    style: PropTypes.object
  }).isRequired,
  imageStyle: PropTypes.object
};

export default PreviewCompatibleImage;
