import React from "react";
import { css } from "@emotion/core";
import Modal from "react-modal";
import { MdClose } from "react-icons/md";
import { push } from "gatsby";

import { colors } from "../style/theme";
import { rhythm } from "../utils/typography";

Modal.setAppElement(`#___gatsby`);

const GatsbyGramModal = ({ location, isOpen, children }) => {
  const breakpoints = [576, 768, 992, 1200];
  const mq = breakpoints.map(bp => `@media (max-width: ${bp}px)`);
  return (
    <Modal
      isOpen={isOpen}
      onRequestClose={() => push("/en/data-archive")}
      style={{
        overlay: {
          position: "fixed",
          top: 0,
          left: 0,
          right: 0,
          bottom: 0,
          backgroundColor: "rgba(0, 0, 0, 0.50)",
          zIndex: 10
        },
        content: {
          marginTop: "3rem",
          position: "absolute",
          border: "none",
          background: "none",
          padding: 0,
          top: 0,
          bottom: 0,
          right: 0,
          left: 0,
          overflow: "auto",
          WebkitOverflowScrolling: "touch"
        }
      }}
      contentLabel="Modal"
    >
      <div onClick={() => push("/en/data-archive")}>
        <div
          css={{
            display: "flex",
            maxWidth: rhythm(47.25),
            margin: "auto",
            width: "100%"
          }}
        >
          {children}
        </div>
        <MdClose
          data-testid="modal-close"
          onClick={() => push("/en/data-archive")}
          css={{
            cursor: "pointer",
            color: "rgba(255,255,255,0.8)",
            fontSize: "30px",
            position: "absolute",
            top: rhythm(1 / 4),
            right: rhythm(1 / 4),
            [mq[0]]: {
              color: colors.dark
            }
          }}
        />
      </div>
    </Modal>
  );
};

export default GatsbyGramModal;
