import React from "react";
import PropTypes from "prop-types";
import Footer from "./Footer";
import Header from "./Header";

function Layout({ children, locale }) {
  return (
    <>
      <Header locale={locale} />
      <main>{children}</main>
      <Footer locale={locale} />
    </>
  );
}

Layout.propTypes = {
  children: PropTypes.oneOfType([PropTypes.object, PropTypes.array])
};

export default Layout;
