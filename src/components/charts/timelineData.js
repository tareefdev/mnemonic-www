import React, { useEffect, useState } from "react";
import useDataApi from "../../components/hooks/useDataApi";
import Timeline from "../../components/charts/timeline";
import useWindowSize from "../../components/hooks/useWindowSize";

function TimelineData({ height, width, api, locale }) {
  const [isMobile, setIsMobile] = useState(false);
  const [windowWidthSize, windowHeightSize] = useWindowSize();
  const [{ data, isLoading, isError }, doFetch] = useDataApi(
    `${api}/collections/?collection=chemical&lang=${locale}`,
    []
  );
  const flexibleWidth = width ? width : isMobile ? 300 : 800;
  useEffect(() => {
    windowWidthSize < 992 ? setIsMobile(true) : setIsMobile(false);
  }, [windowWidthSize]);

  return (
    <>
      {isLoading && (
        <Timeline
          lang={locale}
          incidents={data.data}
          height={height}
          width={flexibleWidth}
          isLoading={isLoading}
        />
      )}
    </>
  );
}

export default TimelineData;
