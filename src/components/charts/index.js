import Timeline from "./timeline";
import TimelineData from "./timelineData";

const charts = { Timeline, TimelineData };

export default charts;
