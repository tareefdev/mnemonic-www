import React from "react";
import PropTypes from "prop-types";
import { jsx } from "@emotion/core";
import PreviewCompatibleImage from "../components/PreviewCompatibleImage.js";
import { rhythm } from "../utils/typography";
import LocalizedLink from "../components/localizedLink";
import { localizedDate, mq } from "../utils/helper";
import { GoGraph } from "react-icons/go";
import { MdInfo } from "react-icons/md";
import { colors } from "../style/theme";
import { IoMdPaper } from "react-icons/io";

function Card({ node, locale }) {
  const { title, image, desc, date } = node.frontmatter;
  const isRTL = locale === "ar";
  return (
    <div
      css={{
        position: "relative",
        maxWidth: rhythm(11.1),
        direction: isRTL ? "rtl" : "ltr",
        [mq[0]]: {
          marginBottom: rhythm(2)
        }
      }}
    >
      <div
        css={{
          display: "flex",
          alignItems: "center",
          marginBottom: rhythm(0.2)
        }}
      >
        <div
          css={{
            display: "flex",
            alignItems: "center"
          }}
        >
          <IoMdPaper
            css={{
              color: colors.primary
            }}
          />
          <small
            css={{
              marginLeft: isRTL ? "0" : rhythm(0.3),
              marginRight: isRTL ? rhythm(0.3) : "0",
              marginTop: rhythm(0.3),
              fontWeight: "bold"
            }}
          >
            Report
          </small>
        </div>
      </div>
      <LocalizedLink
        css={{
          boxShadow: `none`
        }}
        to={`/${node.fields.slug}`}
        locale={locale}
      >
        <PreviewCompatibleImage
          imageInfo={{
            image: image,
            alt: "image"
          }}
          imageStyle={{
            maxWidth: "258px",
            maxHeight: "145px",
            height: "145px",
            objectFit: "cover",
            borderBottom: `6px solid ${colors.dark}`,
            "&::before": {
              content: "''",
              position: "absolute",
              width: "100%",
              height: "100%",
              zIndex: 1,
              top: 0,
              left: isRTL ? "auto" : 0,
              right: isRTL ? 0 : "auto",
              backgroundImage:
                "linear-gradient(rgba(0,0,0,0.63), rgba(175,58,0,0.61))"
            }
          }}
        />
      </LocalizedLink>
      <LocalizedLink
        css={{ boxShadow: `none` }}
        to={`/${node.fields.slug}`}
        locale={locale}
      >
        <h3
          css={{
            position: "absolute",
            top: "55px",
            left: isRTL ? "auto" : "15px",
            right: isRTL ? "15px" : "auto",
            maxWidth: rhythm(11.1),
            color: colors.light,
            zIndex: 3,
            lineHeight: "22px",
            fontSize: "20px",
            fontWeight: "bold",
            margin: 0,
            ":hover, :active": {
              textDecoration: "underline"
            }
          }}
        >
          {title}
        </h3>
      </LocalizedLink>
      <small
        css={{
          display: "inline-block",
          color: colors.primary,
          fontWeight: "bold",
          marginBottom: rhythm(0.4)
        }}
      >
        {localizedDate(date, "d,m,y", locale)}
      </small>
      <p
        css={{
          maxWidth: rhythm(12),
          marginBottom: rhythm(0.5)
        }}
      >
        {desc}
      </p>
    </div>
  );
}

Card.propTypes = {
  node: PropTypes.object,
  locale: PropTypes.string
};

export default Card;
