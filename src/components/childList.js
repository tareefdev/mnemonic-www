import React from "react";
import PropTypes from "prop-types";
import LocalizedLink from "./localizedLink";

function ChildList({ posts }) {
  return (
    <div>
      <h4>Child Posts</h4>
      {posts.map(({ node }) => {
        const title = node.frontmatter.title || node.fields.slug;
        return (
          <div key={node.fields.slug}>
            <LocalizedLink
              css={{ boxShadow: `none` }}
              to={`/${node.fields.slug}`}
            >
              {title}
            </LocalizedLink>
          </div>
        );
      })}
    </div>
  );
}

ChildList.propTypes = {
  posts: PropTypes.object
};

export default ChildList;
