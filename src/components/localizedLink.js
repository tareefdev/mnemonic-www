import React from "react";
import { Link } from "gatsby";
import { locales } from "../config/i18n";

// Use the globally available context to choose the right path
function LocalizedLink({ locale, to, ...props }) {
  const isIndex = to === `/`;
  const path = `${locales[locale].path}/${isIndex ? `` : `${to}/`}`;

  return <Link {...props} to={path} />;
}

export default LocalizedLink;
