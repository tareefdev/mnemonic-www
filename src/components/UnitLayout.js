import React from "react";
import PropTypes from "prop-types";
import Layout from "../components/layout";
import { ModalRoutingContext } from "gatsby-plugin-modal-routing";
import GatsbyGramModal from "./modal";

let windowWidth;

const UnitLayout = ({ location, isModal = false, children }) => {
  if (!windowWidth && typeof window !== `undefined`) {
    windowWidth = window.innerWidth;
  }
  if (isModal && windowWidth > 750) {
    isModal = true;
  }
  //  if (isModal && GatsbyGramModal) {
  return (
    <ModalRoutingContext.Consumer>
      {({ modal, closeTo }) => (
        <div>
          {modal ? (
            <GatsbyGramModal isOpen={true} location={location}>
              {children}
            </GatsbyGramModal>
          ) : (
            <div
              css={{
                background: `rgba(0,0,0,0.03)`,
                minHeight: `100vh`
              }}
            ></div>
          )}
        </div>
      )}
    </ModalRoutingContext.Consumer>
  );
  //  }

  return <Layout>{children}</Layout>;
};

export default UnitLayout;
