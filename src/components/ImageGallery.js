import React from "react";
import PropTypes from "prop-types";
import ImageGallery from "react-image-gallery";
import "react-image-gallery/styles/css/image-gallery.css";

function Gallery({ images }) {
  const items = images.map(image => {
    return { original: image, thumbnail: image };
  });
  return (
    <ImageGallery
      items={items}
      showFullscreenButton={false}
      showPlayButton={false}
    />
  );
}

Gallery.propTypes = {
  images: PropTypes.array
};

export default Gallery;
