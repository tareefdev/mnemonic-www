import React from "react";
import PropTypes from "prop-types";
import { rhythm, scale } from "../utils/typography";
import { css } from "@emotion/core";
import { FaLink } from "react-icons/fa";
import { colors } from "../style/theme";
import { mq } from "../utils/helper";
import useTranslations from "../components/hooks/useTranslations";

function PageHeader({ title, desc, status, locale }) {
  const tr = useTranslations(locale);
  const isRTL = locale === "ar";
  return (
    <div
      css={{
        backgroundImage: "url(/assets/backgroundsmallpattern.png)"
      }}
    >
      <div
        css={{
          display: "grid",
          gridTemplateColumns: "1fr 2fr",
          gridColumnGap: rhythm(2),
          alignItems: "center",
          maxWidth: "990px",
          margin: `0 auto`,
          padding: rhythm(3),
          direction: isRTL ? "rtl" : "ltr",
          [mq[0]]: {
            display: "block",
            padding: rhythm(1.2)
          }
        }}
      >
        <div>
          <h1
            css={{
              ...scale(0.8),
              margin: 0,
              [mq[0]]: {
                ...scale(0.8)
              }
            }}
          >
            {tr(title)}
          </h1>
          <div
            css={{
              borderBottom: `6px solid ${colors.primary}`,
              margin: `${rhythm(0.5)} auto`
            }}
          ></div>
          <span
            css={{
              fontSize: "85%",
              letterSpacing: "0.8",
              fontWeight: 500
            }}
          >
            <FaLink /> {tr(status)}
          </span>
        </div>
        <p
          css={{
            ...scale(0.22),
            letterSpacing: "1.2",
            lineHeight: "27px",
            margin: 0,
            color: "#37363B",
            textAlign: isRTL ? "right" : "left",
            [mq[1]]: {
              marginTop: rhythm(1)
            }
          }}
        >
          {tr(desc)}
        </p>
      </div>
    </div>
  );
}

PageHeader.propTypes = {
  title: PropTypes.string,
  desc: PropTypes.string,
  status: PropTypes.string,

  locale: PropTypes.string
};

export default PageHeader;
