import React from "react";
import PropTypes from "prop-types";
import { css } from "@emotion/core";
import PreviewCompatibleImage from "../components/PreviewCompatibleImage.js";
import { localizedDate, mq } from "../utils/helper";
import { rhythm, scale } from "../utils/typography";
import useTranslations from "../components/hooks/useTranslations";
import { colors } from "../style/theme";

function PostHeader({ post, locale }) {
  const isRTL = locale === "ar";
  const { postType } = post.fields;
  const category = postType.replace(/-/g, " ").toUpperCase();
  const tr = useTranslations(locale);
  return (
    <>
      <div
        css={{
          backgroundImage: 'url("/assets/background-gray.svg")',
          paddingBottom: rhythm(2),
          direction: isRTL ? "rtl" : "ltr",
          [mq[0]]: {
            padding: `${rhythm(2)} ${rhythm(1.3)}`
          }
        }}
      >
        <div
          css={{
            maxWidth: rhythm(34),
            margin: "0 auto",
            paddingTop: rhythm(3.45),
            [mq[0]]: {
              paddingTop: 0
            }
          }}
        >
          <h4
            css={{
              marginTop: 0,
              marginBottom: rhythm(1 / 2),
              color: colors.primary,
              fontWeight: "normal",
              letterSpacing: "0.23px",
              ...scale(1 / 5)
            }}
          >
            {tr(category)}
          </h4>
          <div
            css={{
              paddingBottom: rhythm(0.6),
              marginBottom: rhythm(0.6),
              borderBottom: `6px solid ${colors.primary}`
            }}
          >
            <h1
              css={{
                marginTop: 0,
                marginBottom: rhythm(0.65),
                [mq[0]]: {
                  ...scale(0.8)
                }
              }}
            >
              {post.frontmatter.title}
            </h1>
            <small
              css={{
                paddingTop: rhythm(2),
                fontWeight: "bold",
                color: colors.dark
              }}
            >
              {localizedDate(post.frontmatter.date, "d,m,y", locale)}
            </small>
          </div>
          <p>{post.frontmatter.desc}</p>
        </div>
      </div>
      <div
        css={{
          maxWidth: "800px",
          margin: "4rem auto"
        }}
      >
        <PreviewCompatibleImage
          imageInfo={{
            image: post.frontmatter.image,
            alt: "image"
          }}
          imageStyle={{
            maxHeight: "400px",
            minWidth: "800px"
          }}
        />
      </div>
    </>
  );
}

PostHeader.propTypes = {
  post: PropTypes.object,
  locale: PropTypes.string
};

export default PostHeader;
