import React from "react";
import PropTypes from "prop-types";
import styled from "@emotion/styled";
import { rhythm, scale } from "../../utils/typography";
import { mq } from "../../utils/helper";
import LocalizedLink from "../../components/localizedLink";
import { colors } from "../../style/theme";
import useTranslations from "../../components/hooks/useTranslations";

const CategoryNameRow = styled.div({
  display: "flex",
  justifyContent: "space-between",
  maxWidth: rhythm(47),
  margin: `0 auto`,
  paddingTop: rhythm(2),
  marginBottom: rhythm(2),
  textTransform: "uppercase",
  color: colors.primary,
  fontWeight: "500",
  [mq[0]]: {
    marginBottom: 0
  }
});

const H4 = styled.h4({
  ...scale(1 / 4),
  letterSpacing: "0.23px",
  fontWeight: "normal",
  margin: 0,
  ":hover, :active": {
    textDecoration: "underline"
  },
  [mq[0]]: {
    ...scale(0.4),
    fontWeight: 500,
    marginLeft: rhythm(1),
    marginBottom: rhythm(1)
  }
});

const Small = styled.small({
  fontSize: "85%",
  letterSpacing: "0.88px",
  fontWeight: "bold",
  ":hover, :active": {
    textDecoration: "underline"
  },
  [mq[0]]: {
    display: "none"
  }
});

const PostTitle = styled.h3(props => ({
  marginTop: rhythm(1 / 2),
  position: "relative",
  fontWeight: "bold",
  "&::before": {
    position: "absolute",
    left: props.isRTL ? "0" : "-1.5rem",
    right: props.isRTL ? "-1.5rem" : "0",
    content: '"•"',
    color: colors.primary
  },
  ":hover, :active": {
    textDecoration: "underline"
  }
}));

const PostDesc = styled.p({
  lineHeight: "1rem",
  [mq[0]]: {
    marginBottom: 0
  }
});

const PostColumns = styled.div({
  display: "flex",
  justifyContent: "space-between",
  marginLeft: rhythm(3),
  marginRight: rhythm(3),
  paddingBottom: rhythm(3),
  ".update": {
    width: "30%",
    [mq[0]]: {
      width: "100%",
      marginBottom: rhythm(2)
    }
  },
  [mq[0]]: {
    display: "block",
    marginLeft: rhythm(1),
    marginRight: rhythm(1),
    paddingBottom: 0
  }
});

const PostDate = styled.small({
  fontWeight: "bold",
  color: colors.primary
});

const Box = styled.div({
  maxWidth: rhythm(51.35),
  marginLeft: "auto",
  marginRight: "auto",
  background: colors.light,
  boxShadow: "-5px 5px 15px 0 rgba(0,0,0,0.25)",
  [mq[0]]: {
    background: "rgba(0,0,0,0)",
    boxShadow: "none",
    marginLeft: rhythm(1),
    marginRight: rhythm(1)
  }
});

function PressCoverage({ latestPressUpdates, locale }) {
  const tr = useTranslations(locale);
  const isRTL = locale === "ar";
  return (
    <div
      css={{
        backgroundImage: "url(/assets/backgroundsmallpattern.png)",
        paddingTop: rhythm(4),
        paddingBottom: rhythm(4),
        direction: isRTL ? "rtl" : "ltr",
        "& .press-link": {
          color: colors.primary
        },
        [mq[1]]: {
          paddingTop: rhythm(1),
          paddingBottom: rhythm(1)
        }
      }}
    >
      <Box>
        <CategoryNameRow>
          <H4>
            <LocalizedLink
              to="about/press/"
              className="press-link"
              locale={locale}
            >
              {tr("Press Coverage")}
            </LocalizedLink>
          </H4>
          <Small>
            <LocalizedLink
              to="about/press/"
              className="press-link"
              locale={locale}
            >
              {tr("Read More")} +
            </LocalizedLink>
          </Small>
        </CategoryNameRow>
        <PostColumns>
          {latestPressUpdates.map(post => (
            <div className="update" key={post.id}>
              <a href={post.link} rel="noopener noreferrer" target="_blank">
                <PostTitle isRTL={isRTL}>{post.title}</PostTitle>
              </a>
              <PostDesc>{post.title}</PostDesc>
              <PostDate>{post.date}</PostDate>
            </div>
          ))}
        </PostColumns>
      </Box>
    </div>
  );
}

PressCoverage.propTypes = {
  latestPressUpdates: PropTypes.array
};

export default PressCoverage;
