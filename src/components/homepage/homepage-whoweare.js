import React from "react";
import { jsx } from "@emotion/core";
import styled from "@emotion/styled";
import { colors } from "../../style/theme";
import { rhythm, scale } from "../../utils/typography";
import useTranslations from "../../components/hooks/useTranslations";
import LocalizedLink from "../../components/localizedLink";
import { mq } from "../../utils/helper";

const H2 = styled.h2({
  margin: 0,
  alignSelf: "start",
  width: "80%",
  [mq[1]]: {
    fontWeight: 500,
    marginBottom: rhythm(1),
    marginTop: rhythm(3)
  }
});

const Link = styled.a(props => ({
  color: colors.light,
  textTransform: "uppercase",
  border: `3px solid ${colors.light}`,
  padding: "0.5rem 1rem 0.2rem 1rem",
  fontSize: "85%",
  fontWeight: 500,
  ":hover, :active": {
    color: colors.dark,
    backgroundColor: colors.light
  },
  "&.edge": {
    borderRight: props.isRTL ? "auto" : "none",
    borderLeft: props.isRTL ? "none" : "auto",
    [mq[1]]: {
      display: "block",
      marginBottom: rhythm(1),
      width: "max-content",
      borderRight: `3px solid ${colors.light}`,
      borderLeft: `3px solid ${colors.light}`
    }
  },
  [mq[1]]: {
    display: "inline-block"
  }
}));

const Desc = styled.p({
  ...scale(0.45),
  alignSelf: "start",
  fontWeight: "bold",
  marginBottom: rhythm(0.6),
  [mq[1]]: {
    fontWeight: 500,
    ...scale(0.3)
  }
});

function WhoWeAre({ locale }) {
  const tr = useTranslations(locale);
  const isRTL = locale === "ar";
  return (
    <div
      css={{
        paddingTop: rhythm(6),
        paddingBottom: rhythm(5),
        backgroundColor: colors.dark,
        backgroundImage:
          "linear-gradient(180deg, rgba(0,0,0,0.63) 0%, rgba(175,58,0,0.61) 100%)",
        [mq[1]]: {
          paddingTop: rhythm(1),
          paddingBottom: rhythm(1)
        }
      }}
    >
      <div
        css={{
          display: "grid",
          alignItems: "center",
          gridTemplateColumns: "0.6fr 1.4fr",
          gridTemplateRows: "1fr 1fr 1fr",
          gridRowGap: rhythm(2.5),
          gridTemplateAreas: '"who who-desc" "what what-desc" "lost lost-desc"',
          maxWidth: "950px",
          margin: "auto",
          color: colors.light,
          direction: isRTL ? "rtl" : "ltr",
          [mq[1]]: {
            display: "block",
            margin: `${rhythm(1)} ${rhythm(2)}`
          }
        }}
      >
        <H2
          css={{
            gridArea: "who",
            paddingTop: "1rem",
            [mq[1]]: { marginTop: 0 }
          }}
        >
          {tr("Who we are")}
        </H2>
        <div css={{ gridArea: "who-desc" }}>
          <Desc>{tr("index-page who")}</Desc>
          <Link href="#" rel="noopener noreferrer" target="_blank">
            {tr("Mnemonic")}
          </Link>
        </div>
        <H2 css={{ gridArea: "what" }}>{tr("What we do")}</H2>
        <div css={{ gridArea: "what-desc" }}>
          <Desc>{tr("index-page what")}</Desc>
          <Link
            rel="noopener noreferrer"
            target="_blank"
            href="https://yemeniarchive.org/"
            className="edge"
            isRTL={isRTL}
          >
            {tr("Yemeni Archive")}
          </Link>
          <Link
            href="https://sudanesearchive.org/en"
            rel="noopener noreferrer"
            target="_blank"
          >
            {tr("Sudanese Archive")}
          </Link>
        </div>
        <H2 css={{ gridArea: "lost" }}>{tr("Lost and Found")}</H2>
        <div css={{ gridArea: "lost-desc" }}>
          <Desc>{tr("index-page lost")}</Desc>
          <Link
            as={LocalizedLink}
            to="lost-and-found"
            className="edge"
            isRTL={isRTL}
            locale={locale}
          >
            {tr("Know more")}
          </Link>
          <Link href="mailto:info@syrianarchive.org">{tr("Get in touch")}</Link>
        </div>
      </div>
    </div>
  );
}

export default WhoWeAre;
