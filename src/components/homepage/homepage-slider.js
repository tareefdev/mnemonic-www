import React from "react";
import { jsx } from "@emotion/core";
import PropTypes from "prop-types";
import AwesomeSlider from "react-awesome-slider";
import LocalizedLink from "../../components/localizedLink";
import { rhythm, scale } from "../../utils/typography";
import { IoMdPaper } from "react-icons/io";
import { colors } from "../../style/theme";
import { mq } from "../../utils/helper";
import AwesomeSliderStyles from "../../style/react-awesome-slider.scss";

function Slider({ items, locale }) {
  const isRTL = locale === "ar";
  return (
    <div
      css={{
        " & div": {
          zIndex: 2
        }
      }}
    >
      <AwesomeSlider
        play={false}
        cancelOnInteraction={false}
        interval={1000}
        cssModule={AwesomeSliderStyles}
      >
        {items.map(item => (
          <div
            key={item.id}
            data-alt={item.frontmatter.title}
            data-src={item.frontmatter.image.childImageSharp.fluid.src}
            css={{
              direction: isRTL ? "rtl" : "ltr"
            }}
          >
            <h1
              css={{
                maxWidth: "800px",
                color: colors.light,
                direction: isRTL ? "rtl" : "ltr",
                "& a": {
                  color: colors.light
                },
                "& :hover": {
                  textDecoration: "underline"
                },
                [mq[1]]: {
                  ...scale(0.5),
                  margin: rhythm(1)
                }
              }}
            >
              <LocalizedLink to={item.fields.slug} locale={locale}>
                {item.frontmatter.title}
              </LocalizedLink>
            </h1>
            <div
              css={{
                borderBottom: `1px solid ${colors.primary}`,
                maxWidth: "90%",
                marginLeft: isRTL ? "auto" : 0,
                [mq[1]]: {
                  margin: rhythm(1)
                }
              }}
            ></div>
            <div>
              <div
                css={{
                  display: "flex",
                  alignItems: "center",
                  direction: isRTL ? "rtl" : "ltr",
                  marginTop: rhythm(0.3),
                  color: colors.light,
                  [mq[1]]: {
                    margin: rhythm(1)
                  }
                }}
              >
                <IoMdPaper />
                <small
                  css={{
                    marginLeft: isRTL ? 0 : rhythm(0.3),
                    marginRight: isRTL ? rhythm(0.3) : 0,
                    marginTop: rhythm(0.3),
                    fontWeight: "bold"
                  }}
                >
                  {item.type}
                </small>
              </div>
            </div>
          </div>
        ))}
      </AwesomeSlider>
    </div>
  );
}

Slider.propTypes = {
  items: PropTypes.array
};

export default Slider;
