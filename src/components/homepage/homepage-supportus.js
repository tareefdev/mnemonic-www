import React from "react";
import styled from "@emotion/styled";
import { rhythm, scale } from "../../utils/typography";
import useTranslations from "../../components/hooks/useTranslations";
import LocalizedLink from "../../components/localizedLink";
import { mq } from "../../utils/helper";
import { colors } from "../../style/theme";

const Link = styled.a({
  textTransform: "uppercase",
  border: `3px solid ${colors.dark}`,
  padding: "0.5rem 1rem 0.2rem 1rem",
  fontSize: "85%",
  fontWeight: 500,
  ":hover, :active": {
    backgroundColor: colors.dark,
    color: colors.light
  }
});

function Supportus({ locale }) {
  const tr = useTranslations(locale);
  const isRTL = locale === "ar";
  return (
    <div
      css={{
        backgroundColor: colors.bg,
        display: "flex",
        alignItems: "center",
        justifyContent: "center",
        direction: isRTL ? "rtl" : "ltr"
      }}
    >
      <div
        css={{
          maxWidth: "950px",
          display: "grid",
          gridTemplateColumns: "0.6fr 1.4fr",
          paddingTop: rhythm(4),
          paddingBottom: rhythm(4),
          [mq[1]]: {
            display: "block",
            padding: rhythm(2)
          }
        }}
      >
        <h2
          css={{
            margin: 0,
            [mq[1]]: {
              fontWeight: 500,
              marginBottom: rhythm(1)
            }
          }}
        >
          {tr("Support us")}
        </h2>
        <div>
          <p
            css={{
              ...scale(0.45),
              maxWidth: "670px",
              fontWeight: "bold",
              [mq[1]]: {
                ...scale(0.3)
              }
            }}
          >
            {tr("index-page support")}
          </p>
          <Link
            as={LocalizedLink}
            to="about/team"
            locale={locale}
            css={{
              borderRight: isRTL ? "auto" : "none",
              borderLeft: isRTL ? "none" : "auto"
            }}
          >
            {tr("Work with us")}
          </Link>
          <Link
            href="https://www.patreon.com/syrianarchive"
            rel="noopener noreferrer"
            target="_blank"
          >
            {tr("Donate")}
          </Link>
        </div>
      </div>
    </div>
  );
}

export default Supportus;
