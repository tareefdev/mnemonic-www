import React from "react";
import PropTypes from "prop-types";
import { jsx } from "@emotion/core";
import styled from "@emotion/styled";
import { rhythm, scale } from "../../utils/typography";
import { MdKeyboardArrowRight, MdKeyboardArrowLeft } from "react-icons/md";
import { colors } from "../../style/theme";
import useTranslations from "../../components/hooks/useTranslations";
import LocalizedLink from "../../components/localizedLink";
import { mq } from "../../utils/helper";

const Step = styled.div(props => ({
  display: "flex",
  flexDirection: "column",
  alignItems: "center",
  justifyContent: "center",
  marginLeft: props.isRTL ? rhythm(1.4) : rhythm(0.8),
  marginRight: props.isRTL ? rhythm(1.4) : rhythm(0.8),
  [mq[1]]: {
    display: "block",
    marginLeft: rhythm(2),
    marginBottom: rhythm(1)
  }
}));

const Label = styled.p(props => ({
  ...scale(0.45),
  color: colors.primary,
  letterSpacing: props.isRTL ? "normal" : "1.5px",
  marginBottom: rhythm(0.5),
  textTransform: "uppercase"
}));

const Desc = styled.small({
  margin: 0,
  fontSize: "85%",
  fontWeight: 500,
  opacity: 0.8
});

function Item({ item, locale }) {
  const isRTL = locale === "ar";
  const tr = useTranslations(locale);
  return (
    <>
      <Step isRTL={isRTL}>
        <Label isRTL={isRTL}>{tr(item.label)}</Label>
        <Desc>
          {item.num} {tr(item.desc)}
        </Desc>
      </Step>
      {isRTL ? (
        <MdKeyboardArrowLeft
          css={{
            fontSize: "24px",
            opacity: 0.6,
            ":last-of-type": {
              display: "none"
            },
            [mq[1]]: {
              display: "none"
            }
          }}
        />
      ) : (
        <MdKeyboardArrowRight
          css={{
            fontSize: "24px",
            opacity: 0.6,
            ":last-of-type": {
              display: "none"
            },
            [mq[1]]: {
              display: "none"
            }
          }}
        />
      )}
    </>
  );
}

function Workflow({ items, locale }) {
  const tr = useTranslations(locale);
  const isRTL = locale === "ar";
  return (
    <>
      <div
        css={{
          display: "flex",
          alignItems: "center",
          justifyContent: "center",
          flexDirection: "column",
          backgroundColor: colors.light,
          maxWidth: "950px",
          margin: "auto",
          direction: isRTL ? "rtl" : "ltr"
        }}
      >
        <h2
          css={{
            alignSelf: "start",
            marginTop: rhythm(3),
            marginBottom: rhythm(2),
            [mq[1]]: {
              margin: rhythm(2),
              marginBottom: rhythm(1)
            }
          }}
        >
          {tr("How we do it")}
        </h2>
        <div
          css={{
            backgroundImage: "url(/assets/homepage-graph.svg)",
            backgroundRepeat: "no-repeat",
            height: "250px",
            width: "100%",
            marginBottom: rhythm(3),
            transform: isRTL ? "scaleX(-1)" : "none",
            filter: isRTL ? "FlipH" : "none",
            [mq[1]]: {
              display: "none"
            }
          }}
        ></div>
        <div
          css={{
            width: "100%",
            display: "flex",
            alignItems: "center",
            justifyContent: "center",
            [mq[1]]: {
              display: "block"
            }
          }}
        >
          {items.map(item => (
            <Item key={item.id} item={item} locale={locale} />
          ))}
        </div>
        <LocalizedLink
          to="tools-methods"
          css={{
            fontSize: "85%",
            letterSpacing: isRTL ? "normal" : "0.88px",
            fontWeight: 500,
            lineHeight: "16px",
            marginTop: rhythm(2),
            marginBottom: rhythm(2),
            border: "3px solid rgba(0,0,0,0.8)",
            padding: `0.3rem 2rem 0.1rem 2rem`,
            ":hover, :active": {
              backgroundColor: colors.dark,
              color: colors.light
            }
          }}
          locale={locale}
        >
          {tr("Explore our methodology")}
        </LocalizedLink>
      </div>
    </>
  );
}

Item.propTypes = {
  item: PropTypes.object,
  isRTL: PropTypes.bool
};

export default Workflow;
