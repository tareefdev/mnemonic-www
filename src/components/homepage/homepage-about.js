import React from "react";
import PropTypes from "prop-types";
import { jsx } from "@emotion/core";
import { rhythm, scale } from "../../utils/typography";
import useTranslations from "../../components/hooks/useTranslations";
import { colors } from "../../style/theme";
import { mq } from "../../utils/helper";

function About({ locale }) {
  const tr = useTranslations(locale);
  const isRTL = locale === "ar";
  return (
    <div
      css={{
        backgroundImage: "url(/assets/homepage-about.png)",
        backgroundRepeat: "no-repeat",
        backgroundSize: "16%",
        backgroundColor: colors.bg,
        display: "flex",
        alignItems: "center",
        justifyContent: "center",
        paddingTop: rhythm(2),
        paddingBottom: rhythm(2),
        direction: isRTL ? "rtl" : "ltr",
        transform: isRTL ? "scaleX(-1)" : "none",
        filter: isRTL ? "FlipH" : "none",
        [mq[0]]: {
          paddingTop: 0,
          paddingBottom: 0,
          backgroundSize: "40%"
        }
      }}
    >
      <div
        css={{
          transform: isRTL ? "scaleX(-1)" : "none",
          filter: isRTL ? "FlipH" : "none",
          maxWidth: "950px",
          ...scale(0.45),
          fontWeight: "bold",
          [mq[1]]: {
            ...scale(0.25),
            margin: rhythm(2)
          }
        }}
      >
        {tr("homepage about")}
      </div>
    </div>
  );
}

export default About;

About.propTypes = {
  intro: PropTypes.string
};
