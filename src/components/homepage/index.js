import Slider from "./homepage-slider";
import Workflow from "./homepage-workflow";
import About from "./homepage-about";
import WhoWeAre from "./homepage-whoweare";
import Supportus from "./homepage-supportus";
import PressCoverage from "./homepage-press-coverage";

const homepage = {
  Slider,
  Workflow,
  About,
  WhoWeAre,
  Supportus,
  PressCoverage
};

export default homepage;
