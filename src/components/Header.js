import React, { useState, Fragment } from "react";
import PropTypes from "prop-types";
import LocalizedLink from "../components/localizedLink";
import useTranslations from "../components/hooks/useTranslations";
import { MdMenu, MdClose } from "react-icons/md";
import styled from "@emotion/styled";
import { rhythm, scale } from "../utils/typography";
import { colors } from "../style/theme";
import { jsx } from "@emotion/core";
import { mq } from "../utils/helper";

const assignActiveStyles = ({ isPartiallyCurrent }) =>
  isPartiallyCurrent ? { style: { color: "colors.primary" } } : {};

const Item = styled.li(props => ({
  margin: 0,
  paddingLeft: props.mobile ? 0 : rhythm(2),
  marginLeft: props.mobile ? rhythm(1.2) : 0,
  paddingTop: props.mobile ? rhythm(1.3) : "initial",
  textTransform: "uppercase",
  "a:hover, a:active": {
    color: colors.primary
  }
}));

const navItems = [
  { name: "Investigations", to: "investigations", id: 0 },
  { name: "The Archive", to: "data-archive", id: 1 },
  { name: "Lost and Found", to: "lost-and-found", id: 2 },
  { name: "Tools and Methods", to: "tools-methods", id: 3 },
  { name: "About", to: "about", id: 4 }
];

function MobileNav({ tr, isRTL, locale }) {
  const mobileSecondNav = [
    { name: "Press", to: "", id: 0 },
    { name: "Team", to: "", id: 1 },
    { name: "Legal", to: "", id: 2 },
    { name: "Donate", to: "", id: 3 }
  ];
  return (
    <nav
      css={{
        Minheight: "75vh"
      }}
    >
      <ul
        css={{
          listStyle: "none",
          "li: last-of-type": {
            marginTop: rhythm(1),
            padding: rhythm(1 / 4),
            display: "inline-block",
            border: `3px solid ${colors.dark}`
          },
          [mq[1]]: {
            marginRight: isRTL ? rhythm(2) : 0
          }
        }}
      >
        {navItems.map(item => (
          <Item mobile isRTL={isRTL} key={item.id}>
            <LocalizedLink
              to={item.to}
              getProps={assignActiveStyles}
              locale={locale}
            >
              {tr(item.name)}
            </LocalizedLink>
          </Item>
        ))}
        <div
          css={{
            borderBottom: `3px solid ${colors.primary}`,
            width: "70%",
            margin: `${rhythm(1)} auto`,
            marginBottom: 0
          }}
        ></div>
        {mobileSecondNav.map(item => (
          <Item mobile key={item.id}>
            <LocalizedLink to={item.to} locale={locale}>
              {tr(item.name)}
            </LocalizedLink>
          </Item>
        ))}
      </ul>
    </nav>
  );
}

function MobileHeader({ tr, isRTL, locale }) {
  const [isNavCollapsed, setIsNavCollapsed] = useState(false);

  return (
    <header
      css={{
        display: "none",
        position: "sticky",
        top: 0,
        let: 0,
        backgroundColor: colors.light,
        borderBottom: `3px solid ${colors.dark}`,
        zIndex: 10,
        direction: isRTL ? "rtl" : "ltr",
        [mq[2]]: {
          display: "block",
          paddingTop: rhythm(0.3),
          paddingBottom: rhythm(0.3)
        }
      }}
    >
      <div
        css={{
          display: "flex",
          alignItems: "center",
          width: "100%",
          padding: rhythm(0.25)
        }}
      >
        <div
          css={{
            display: "flex",
            alignItems: "center",
            textTransform: "uppercase"
          }}
        >
          <img
            src="/assets/header-logo.svg"
            height="100%"
            alt="logo"
            css={{ marginBottom: 0 }}
          />
          <LocalizedLink
            css={{
              ...scale(1 / 2),
              position: "relative",
              top: "8%",
              marginRight: isRTL ? rhythm(1) : "auto",
              marginLeft: isRTL ? "auto" : rhythm(1),
              fontWeight: "bold",
              [mq[0]]: {
                ...scale(0.3)
              }
            }}
            to={""}
            locale={locale}
          >
            {tr("Syrian Archive")}
          </LocalizedLink>
        </div>
        <div
          onClick={() => setIsNavCollapsed(!isNavCollapsed)}
          css={{
            cursor: "pointer",
            color: colors.dark,
            marginLeft: isRTL ? 0 : "auto",
            marginRight: isRTL ? "auto" : 0,
            fontSize: "130%"
          }}
        >
          {isNavCollapsed ? <MdClose /> : <MdMenu />}
        </div>
      </div>
      {isNavCollapsed && <MobileNav tr={tr} isRTL={isRTL} locale={locale} />}
    </header>
  );
}

function DesktopNav({ tr, locale }) {
  return (
    <nav
      css={{
        display: "flex",
        alignItems: "center",
        [mq[2]]: {
          display: "none"
        }
      }}
    >
      <ul
        css={{
          display: "flex",
          alignItems: "center",
          listStyleType: "none",
          margin: 0
        }}
      >
        {navItems.map(item => (
          <Item key={item.id}>
            <LocalizedLink
              to={item.to}
              getProps={assignActiveStyles}
              locale={locale}
            >
              {tr(item.name)}
            </LocalizedLink>
          </Item>
        ))}
        <Item
          css={{
            display: "flex",
            alignItems: "center",
            "a:hover, a:active": {
              backgroundColor: colors.dark,
              color: colors.light
            }
          }}
        >
          <a
            href="https://www.patreon.com/syrianarchive"
            rel="noopener noreferrer"
            target="_blank"
            css={{
              border: `3px solid ${colors.dark}`,
              padding: "0.5rem 0.4rem 0.2rem 0.4rem"
            }}
          >
            {tr("Donate Now")}
          </a>
        </Item>
      </ul>
    </nav>
  );
}

function DesktopHeader({ tr, isRTL, locale }) {
  return (
    <header
      css={{
        position: "sticky",
        width: "100%",
        top: 0,
        let: 0,
        backgroundColor: colors.light,
        zIndex: 10,
        direction: isRTL ? "rtl" : "ltr",
        [mq[2]]: {
          display: "none"
        }
      }}
    >
      <div
        css={{
          display: "flex",
          justifyContent: "space-between",
          alignItems: "center",
          minHeight: rhythm(4.3),
          position: "relative",
          padding: `${rhythm(0.68)} ${rhythm(2)}`,
          boxShadow: "-3px 3px 10px 0 rgba(0,0,0,0.08)",
          textTransform: "uppercase",
          fontWeight: "500",
          [mq[0]]: {
            padding: rhythm(0.25),
            minHeight: rhythm(3)
          }
        }}
      >
        <a
          css={{
            display: "flex",
            alignItems: "center",
            justifyContent: "center",
            [mq[0]]: {
              marginLeft: rhythm(1)
            }
          }}
          href={isRTL ? "/ar" : "/"}
        >
          <img
            src="/assets/header-logo.svg"
            alt="logo"
            height="100%"
            css={{ marginBottom: 0 }}
          />
          <div
            css={{
              transform: "translate(0%, 15%)",
              ...scale(1 / 2),
              marginRight: isRTL ? rhythm(0.5) : "auto",
              marginLeft: isRTL ? "auto" : rhythm(0.5),
              fontWeight: "bold",
              [mq[0]]: {
                ...scale(0.3)
              }
            }}
          >
            {tr("Syrian Archive")}
          </div>
        </a>
        <DesktopNav tr={tr} locale={locale} />
      </div>
    </header>
  );
}

function Header({ locale }) {
  const tr = useTranslations(locale);
  const isRTL = locale === "ar";

  return (
    <Fragment>
      <DesktopHeader tr={tr} isRTL={isRTL} locale={locale} />
      <MobileHeader tr={tr} isRTL={isRTL} locale={locale} />
    </Fragment>
  );
}

MobileNav.propTypes = {
  tr: PropTypes.func
};

MobileHeader.propTypes = {
  tr: PropTypes.func,
  isRTL: PropTypes.bool
};

DesktopNav.propTypes = {
  tr: PropTypes.func
};

DesktopHeader.propTypes = {
  tr: PropTypes.func,
  isRTL: PropTypes.bool
};

export default Header;
