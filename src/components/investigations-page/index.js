import Datasets from "./datasets";
import Reports from "./reports";

const investigations = { Datasets, Reports };

export default investigations;
