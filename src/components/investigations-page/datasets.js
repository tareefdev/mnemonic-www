import React from "react";
import PropTypes from "prop-types";
import styled from "@emotion/styled";
import { colors } from "../../style/theme";
import { rhythm } from "../../utils/typography";
import { mq } from "../../utils/helper";
import Card from "../../components/card";
import useTranslations from "../../components/hooks/useTranslations";

const H2 = styled.h2({
  margin: 0,
  marginBottom: rhythm(2),
  borderBottom: `6px solid ${colors.primary}`,
  display: "inline-block"
});

function Datasets({ nodes, locale }) {
  const isRTL = locale === "ar";
  const tr = useTranslations(locale);
  return (
    <div
      css={{
        backgroundColor: colors.bg,
        direction: isRTL ? "rtl" : "ltr"
      }}
    >
      <div
        css={{
          maxWidth: rhythm(50),
          margin: "0 auto",
          paddingTop: rhythm(1.5),
          paddingBottom: rhythm(1.5),
          [mq[1]]: {
            padding: rhythm(1.2)
          }
        }}
      >
        <H2>{tr("Data Sets")}</H2>
        <div
          css={{
            display: "grid",
            gridTemplateColumns: "1fr 1fr",
            [mq[1]]: {
              display: "block"
            }
          }}
        >
          {nodes.map(({ node }) => (
            <Card node={node} key={node.fields.slug} locale={locale} />
          ))}
        </div>
      </div>
    </div>
  );
}

Datasets.propTypes = {
  nodes: PropTypes.array,
  locale: PropTypes.string
};

export default Datasets;
