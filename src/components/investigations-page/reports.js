import React from "react";
import PropTypes from "prop-types";
import styled from "@emotion/styled";
import { groupBy, map } from "lodash";
import { colors } from "../../style/theme";
import { rhythm, scale } from "../../utils/typography";
import { mq } from "../../utils/helper";
import Card from "../../components/card";
import useTranslations from "../../components/hooks/useTranslations";

const H2 = styled.h2({
  margin: 0,
  marginBottom: rhythm(2),
  borderBottom: `6px solid ${colors.primary}`,
  display: "inline-block"
});

function Reports({ nodes, locale }) {
  const isRTL = locale === "ar";
  const tr = useTranslations(locale);
  const groupedPosts = map(
    groupBy(nodes, n => n.node.frontmatter.dataset),
    g => [g[0].node.frontmatter.dataset, g]
  );
  return (
    <div
      css={{
        backgroundColor: colors.light,
        direction: isRTL ? "rtl" : "ltr"
      }}
    >
      <div
        css={{
          maxWidth: rhythm(50),
          margin: "0 auto",
          paddingTop: rhythm(1.5),
          paddingBottom: rhythm(1.5),
          [mq[1]]: {
            padding: rhythm(1.2)
          }
        }}
      >
        <H2>{tr("Investigations")}</H2>
        <div>
          {groupedPosts.map(node => (
            <div key={node[0]}>
              <h3
                css={{
                  ...scale(0.6),
                  margin: 0,
                  marginBottom: "1rem",
                  textTransform: "capitalize"
                }}
              >
                {node[0]}
              </h3>
              <div
                css={{
                  display: "grid",
                  gridTemplateColumns: "repeat(4, 1fr)",
                  gridRowGap: rhythm(3),
                  gridColumnGap: rhythm(2),
                  paddingBottom: rhythm(1),
                  direction: isRTL ? "rtl" : "ltr",
                  [mq[0]]: {
                    display: "block"
                  }
                }}
              >
                {node[1].map(({ node }) => (
                  <Card node={node} key={node.fields.slug} locale={locale} />
                ))}
              </div>
              <hr
                css={{
                  backgroundColor: colors.primary
                }}
              />
            </div>
          ))}
        </div>
      </div>
    </div>
  );
}

Reports.propTypes = {
  nodes: PropTypes.array,
  locale: PropTypes.string
};

export default Reports;
