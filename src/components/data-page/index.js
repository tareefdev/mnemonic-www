import SearchForm from "./search-form";
import ListData from "./list-data";

const dataArchive = { SearchForm, ListData };

export default dataArchive;
