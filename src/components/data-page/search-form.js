import React, { useState } from "react";
import PropTypes from "prop-types";
//import DayPickerInput from "react-day-picker/DayPickerInput";
import { colors } from "../../style/theme";
import { isUndefined, uniq, sortBy } from "lodash";
import { rhythm } from "../../utils/typography";
import useTranslations from "../hooks/useTranslations";
import styled from "@emotion/styled";
import { mq } from "../../utils/helper";
//import "../../style/react-day-picker.css";
import { enGB } from "date-fns/locale";
import { DatePicker } from "react-nice-dates";
import styles from "../../style/react-nice-dates.scss";
// require("react-nice-dates/build/style.css");
import cn from "classnames/bind";

const cx = cn.bind(styles);

const Input = styled.input({
  border: `4px solid ${colors.dark}`,
  width: rhythm(10),
  height: "2.5rem",
  padding: "0.4rem",
  paddingTop: "0.5rem",
  "::placeholder": {
    fontWeight: "bold",
    color: colors.dark,
    opacity: 1
  },
  [mq[0]]: {
    minWidth: rhythm(10),
    margin: "0 auto"
  }
});

const FormOption = styled.div({
  marginBottom: rhythm(1),
  "& .DayPickerInput input": {
    "::placeholder": {
      fontWeight: "bold",
      color: colors.dark,
      opacity: 1
    },
    height: "1.8rem",
    padding: "0.4rem 0.3rem 0.3rem 0.3rem"
  }
});

const dateOrString = date => (isUndefined(date) ? "" : date);

function SearchForm({ submitAction, currentPage, pageCount, ApiURL, locale }) {
  const [date, setDate] = useState();
  const [query, setQuery] = useState({
    title: "",
    location: "",
    dateBefore: "",
    dateAfter: ""
  });
  const tr = useTranslations(locale);
  const pages = [1, pageCount];
  pages.push(currentPage - 1);
  pages.push(currentPage);
  pages.push(currentPage + 1);
  const existPages = uniq(sortBy(pages.filter(p => p > 0 && p <= pageCount)));

  return (
    <form
      onSubmit={event => {
        event.preventDefault();
        submitAction(
          `${ApiURL}/search/?title=${query.title}&location=${query.location}&dateBefore=${query.dateBefore}&dateAfter=${query.dateAfter}&lang=${locale}`
        );
      }}
      css={{
        position: "sticky",
        top: rhythm(6)
      }}
    >
      <FormOption>
        <Input
          type="text"
          value={query.title}
          onChange={event => setQuery({ ...query, title: event.target.value })}
          placeholder={tr("Title")}
        />
      </FormOption>
      <FormOption>
        <select
          css={{
            fontWeight: "bold",
            backgroundColor: colors.light,
            height: "2.3rem",
            padding: "0.4rem",
            border: `4px solid ${colors.dark}`,
            width: rhythm(10)
          }}
        >
          <option value="volvo">Volvo</option>
          <option value="saab">Saab</option>
          <option value="opel">Opel</option>
          <option value="audi">Audi</option>
        </select>
      </FormOption>
      <FormOption>
        {/* <DayPickerInput */}
        {/*   onDayChange={day => */}
        {/*     setQuery({ ...query, dateBefore: dateOrString(day) }) */}
        {/*   } */}
        {/* /> */}
      </FormOption>
      <FormOption>
        <div className={cx(styles)}>
          <DatePicker date={date} onDateChange={setDate} locale={enGB}>
            {({ inputProps, focused }) => (
              <input
                className={"input" + (focused ? " -focused" : "")}
                {...inputProps}
              />
            )}
          </DatePicker>
        </div>
        {/* <DayPickerInput */}
        {/*   onDayChange={day => */}
        {/*     setQuery({ ...query, dateAfter: dateOrString(day) }) */}
        {/*   } */}
        {/* /> */}
      </FormOption>
      <FormOption>
        <select
          css={{
            fontWeight: "bold",
            backgroundColor: colors.light,
            height: "2.3rem",
            padding: "0.4rem",
            border: `4px solid ${colors.dark}`,
            width: rhythm(10)
          }}
        >
          <option value="opel">Opel</option>
          <option value="audi">Audi</option>
        </select>
      </FormOption>
      <FormOption>
        <Input
          type="text"
          value={query.location}
          onChange={e => setQuery({ ...query, location: e.target.value })}
          placeholder={tr("Location")}
        />
      </FormOption>
      <button
        type="submit"
        css={{
          display: "block",
          border: `3px solid ${colors.dark}`,
          padding: "0.4rem 0.6rem 0.14rem 0.6rem",
          fontWeight: "bold",
          backgroundColor: colors.light,
          ":hover, :active": {
            backgroundColor: colors.dark,
            color: colors.light,
            cursor: "pointer"
          }
        }}
      >
        {tr("Search")}
      </button>
      <div
        css={{
          borderTop: `2px solid ${colors.dark}`,
          marginTop: rhythm(1),
          paddingTop: rhythm(0.7)
        }}
      >
        <span
          css={{
            display: "inline-block",
            marginBottom: rhythm(0.5)
          }}
        >
          {tr("Page")} {currentPage} {tr("of")} {pageCount}
        </span>
        <ul
          css={{
            listStyle: "none",
            display: "flex",
            justifyContent: "space-between"
          }}
        >
          {existPages.map(page => (
            <li
              css={{
                cursor: "pointer",
                padding: `${rhythm(0, 1)} ${rhythm(0.2)}`,
                border: `1px solid ${colors.dark}`
              }}
              key={page}
              onClick={() => {
                submitAction(
                  `${ApiURL}/search/?title=${query.title}&location=${query.location}&dateBefore=${query.dateBefore}&dateAfter=${query.dateAfter}&lang=${locale}&page=${page}`
                );
              }}
            >
              {page}
            </li>
          ))}
        </ul>
      </div>
    </form>
  );
}

SearchForm.propTypes = {
  submitAction: PropTypes.func,
  pageCount: PropTypes.number,
  currentPage: PropTypes.number,
  ApiURL: PropTypes.string,
  locale: PropTypes.string
};

export default SearchForm;
