import React from "react";
import PropTypes from "prop-types";
import styled from "@emotion/styled";
import { MdLocationOn } from "react-icons/md";
import { colors } from "../../style/theme";
import { rhythm, scale } from "../../utils/typography";
import LocalizedLink from "../localizedLink";
import useTranslations from "../hooks/useTranslations";
import { FaRegCalendarAlt, FaCheck } from "react-icons/fa";
import { localizedDate, mq } from "../../utils/helper";

const P = styled.p({
  marginBottom: 0,
  fontSize: "85%"
});

function ListData({ units, locale }) {
  const tr = useTranslations(locale);
  return (
    <div
      css={{
        backgroundColor: colors.bg,
        "& div": {
          marginLeft: rhythm(0.8),
          marginRight: rhythm(0.5)
        },
        "& svg": {
          marginRight: rhythm(0.2)
        }
      }}
    >
      {units.map(item => (
        <div
          key={item.id}
          css={{
            display: "flex",
            justifyContent: "space-between",
            backgroundColor: colors.light,
            marginBottom: rhythm(1),
            padding: `${rhythm(1.3)} ${rhythm(1)}`,

            ":hover, :active": {
              boxShadow: "0 2px 3px 0 rgba(0,0,0,0.25)",
              cursor: "pointer"
            },
            [mq[0]]: {
              flexDirection: "column"
            }
          }}
        >
          <div
            css={{
              display: "flex",
              flexDirection: "column",
              justifyContent: "space-between",
              [mq[0]]: {
                flexDirection: "row",
                marginBottom: rhythm(0.5)
              }
            }}
          >
            <P>
              <FaRegCalendarAlt />
              {localizedDate(item["incident_date_time"], "y,M,d", locale)}
            </P>
            <P>
              <MdLocationOn />
              {item.location.name}
            </P>
          </div>
          <div
            css={{
              ...scale(0.25),
              fontWeight: "bold"
            }}
          >
            <LocalizedLink
              to={`/database/unit/${item.id}`}
              locale={locale}
              state={{
                modal: true
              }}
            >
              {item.title}
            </LocalizedLink>
          </div>
          <div
            css={{
              display: "flex",
              flexDirection: "column",
              justifyContent: "space-between",
              alignItems: "flex-end",
              [mq[0]]: {
                marginTop: rhythm(0.5),
                flexDirection: "row-reverse"
              }
            }}
          >
            <P
              css={{
                alignSelf: "start"
              }}
            >
              <span css={{ textTransform: "uppercase" }}>{tr("verified")}</span>{" "}
              <FaCheck />
            </P>
            <P>
              <span css={{ display: "inline-block" }}>{tr("Incident")}:</span>
              <span css={{ color: colors.primary }}>R-SY040401_300915</span>
            </P>
          </div>
        </div>
      ))}
    </div>
  );
}

ListData.propTypes = {
  units: PropTypes.array,
  locale: PropTypes.string
};

export default ListData;
