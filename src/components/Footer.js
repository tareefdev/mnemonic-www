import React from "react";
import { jsx } from "@emotion/core";
import styled from "@emotion/styled";
import { FaFacebookSquare, FaTwitterSquare, FaGithub } from "react-icons/fa";
import useTranslations from "./hooks/useTranslations";
//import MailingList from "./mailing-list";
import LocalizedLink from "./localizedLink";
import { colors } from "../style/theme";
import { rhythm } from "../utils/typography";
import { mq } from "../utils/helper";

const P = styled.p(props => ({
  color: colors.light,
  fontSize: "85%",
  letterSpacing: props.isRTL ? "normal" : "0.77px",
  lineHeight: "24px"
}));

const Link = styled.a({
  fontSize: "85%",
  fontWeight: 500,
  textTransform: "uppercase",
  color: colors.light,
  display: "block",
  ":hover": {
    textDecoration: "underline"
  }
});

function Footer({ locale }) {
  const isRTL = locale === "ar";
  const tr = useTranslations(locale);

  return (
    <footer
      css={{
        width: "100%",
        backgroundColor: colors.dark,
        paddingTop: rhythm(3),
        paddingBottom: rhythm(2),
        direction: isRTL ? "rtl" : "ltr",
        [mq[0]]: {
          paddingTop: rhythm(0.5)
        }
      }}
    >
      <div
        css={{
          display: "grid",
          gridTemplateColumns: "1.3fr 0.6fr 0.7fr 1fr",
          gridTemplateAreas: '"syrianarchive links mnemonic social"',
          gridColumnGap: rhythm(1),
          margin: "0 auto",
          maxWidth: rhythm(51.35),
          color: colors.light,
          "& li": {
            listStyle: "none"
          },
          [mq[0]]: {
            display: "block",
            margin: rhythm(1)
          }
        }}
      >
        <div css={{ gridArea: "syrianarchive", maxWidth: "80%" }}>
          <div
            css={{
              display: "flex",
              alignItems: "center",
              marginBottom: rhythm(1),
              [mq[0]]: {}
            }}
          >
            <a href={isRTL ? "/ar" : "/"}>
              <img
                src="/assets/header-logo.svg"
                height="10%"
                alt="logo"
                css={{
                  marginBottom: 0
                }}
              />
            </a>
            <a href={isRTL ? "/ar" : "/"}>
              <h3
                css={{
                  margin: 0,
                  marginLeft: isRTL ? "auto" : rhythm(1),
                  marginRight: isRTL ? rhythm(1) : "auto",
                  color: colors.light
                }}
              >
                {tr("Syrian Archive")}
              </h3>
            </a>
          </div>
          <div>
            <P isRTL={isRTL}>{tr("about-page support")}</P>
            <a
              href="https://www.patreon.com/syrianarchive"
              rel="noopener noreferrer"
              target="_blank"
              css={{
                color: colors.light,
                border: `3px solid ${colors.light}`,
                padding: "0.4rem 0.9rem 0.2rem 0.9rem",
                ":hover, :active": {
                  backgroundColor: colors.light,
                  color: colors.dark
                }
              }}
            >
              {tr("Donate")}
            </a>
          </div>
        </div>

        <div
          css={{
            gridArea: "links",
            display: "flex",
            flexDirection: "column",
            justifyContent: "space-between",
            height: "120px",
            alignSelf: "center",
            [mq[1]]: {
              marginTop: rhythm(2),
              marginBottom: rhythm(2)
            }
          }}
        >
          <Link as={LocalizedLink} to="about" locale={locale}>
            {tr("about")}
          </Link>
          <Link as={LocalizedLink} to="about/contact" locale={locale}>
            {tr("contact")}
          </Link>
          <Link as={LocalizedLink} to="about/press" locale={locale}>
            {tr("press")}
          </Link>
          <Link as={LocalizedLink} to="about/legal" locale={locale}>
            {tr("legal stuff")}
          </Link>
        </div>

        <div
          css={{
            gridArea: "mnemonic",
            display: "flex",
            flexDirection: "column",
            justifyContent: "space-between",
            height: "120px",
            alignSelf: "center"
          }}
        >
          <Link
            href="#"
            css={{ color: "#27FFFF" }}
            rel="noopener noreferrer"
            target="_blank"
          >
            {tr("Mnemonic")}
          </Link>
          <Link
            href="https://sudanesearchive.org/"
            rel="noopener noreferrer"
            target="_blank"
          >
            {tr("Sudanese Archive")}
          </Link>
          <Link
            href="https://yemeniarchive.org/"
            rel="noopener noreferrer"
            target="_blank"
          >
            {tr("Yemeni Archive")}
          </Link>
        </div>

        <div
          css={{
            gridArea: "social",
            height: "120px",
            alignSelf: "center",
            [mq[1]]: {
              marginTop: rhythm(2)
            }
          }}
        >
          <div
            css={{
              fontSize: "2.2rem",
              display: "flex",
              justifyContent: "space-evenly",
              marginTop: rhythm(1.2)
            }}
          >
            <Link
              aria-label="facebook account"
              href="https://facebook.com/syrianarchive"
            >
              <FaFacebookSquare />
            </Link>
            <Link
              aria-label="twitter account"
              href="https://twitter.com/syrian_archive"
            >
              <FaTwitterSquare />
            </Link>
            <Link
              aria-label="github account"
              href="https://github.com/syrianarchive/"
            >
              <FaGithub />
            </Link>
          </div>
        </div>
      </div>
    </footer>
  );
}

export default Footer;
