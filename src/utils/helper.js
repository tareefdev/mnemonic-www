function localizedDate(unixStampedDate, format, locale) {
  const date = new Date(unixStampedDate);
  const isRTL = locale === "ar";
  const options = {
    ...(format.includes("d") && { day: "numeric" }),
    ...(format.includes("m") && { month: "long" }),
    ...(format.includes("M") && { month: "2-digit" }),
    ...(format.includes("y") && { year: "numeric" }),
    ...(format.includes("w") && { weekday: "long" })
  };
  return isRTL
    ? date.toLocaleDateString("ar-SY", options)
    : date.toLocaleDateString("en-US", options);
}

const breakpoints = [576, 768, 992, 1280, 1400];
const mq = breakpoints.map(bp => `@media (max-width: ${bp}px)`);

const helper = { localizedDate, mq };

export default helper;
export { localizedDate, mq };
