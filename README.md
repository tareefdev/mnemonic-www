mnemonic-www
============

Homepage
========

Slider:
-------

```javascript
<Slider items={sliderItems} />
```

Take mdx nodes items as arguments and return a Slider component based on
react-awesome-slider.

About
-----

```javascript
<About />
```

require `/assets/homepage-about.png` image

Workflow
--------

```javascript
<Workflow items={workflowItems} />
```

Take an array of objects like

```javascript
{ label: "collect", num: "5,024", desc: "Sources", id: "0" }
```

and return a Workflow component which **require** an image:
`/assets/homepage-graph.svg`

Whoweare
--------

```javascript
<WhoWeAre />
```

Supportus
=========

```javascript
<Supportus />
```

PressConverage
==============

```javascript
PressCoverage latestPressUpdates={latestPressUpdates} />
```

Take mdx nodes items as arguments and return a Slider component based on
react-awesome-slider.
