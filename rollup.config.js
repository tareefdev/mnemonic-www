import babel from "rollup-plugin-babel";
import external from "rollup-plugin-peer-deps-external";
import resolve from "@rollup/plugin-node-resolve";
import commonjs from "@rollup/plugin-commonjs";
import builtins from "rollup-plugin-node-builtins";
import globals from "rollup-plugin-node-globals";
import json from "@rollup/plugin-json";
import postcss from "rollup-plugin-postcss";
import { terser } from "rollup-plugin-terser";

const input = "./src/index.js";

export default [
  {
    onwarn: function(warning, warn) {
      if (warning.code === "CIRCULAR_DEPENDENCY") return;
      warn(warning);
    },
    input,
    output: {
      file: "lib/index.js",
      format: "umd",
      sourcemap: true,
      name: "mnemonics-www",
      external: ["react", "@emotion/styled", "@emotion/core"],
      globals: {
        react: "React",
        "@emotion/styled": "styled",
        "@emotion/core": "core",
        "prop-types": "PropTypes",
        gatsby: "gatsby",
        lodash: "lodash",
        typography: "Typography",
        "gatsby-image": "Img",
        "react-dom": "reactDom"
      }
    },
    plugins: [
      external(),
      json(),
      postcss({
        extract: false,
        modules: true
      }),
      babel({
        runtimeHelpers: true,
        exclude: "node_modules/**"
      }),
      resolve({ preferBuiltins: true, browser: true }),
      commonjs({
        namedExports: {
          "node_modules/lodash/lodash.js": [
            "map",
            "groupBy",
            "isUndefined",
            "uniq",
            "sortBy",
            "find"
          ]
        }
      }),
      globals(),
      builtins(),
      terser()
    ]
  }
];
